#!/bin/bash
#centos 6.4 install

display_help() {
    echo "install_centos.sh [options]"
    echo ""
    echo "--base, -b URL    Use URL as base URL for antiSMASH tarball"
    echo "--help, -h        Display this help"
}

while true; do
    case "$1" in
        -h|--help) display_help; exit 0;;
        -b|--base) ANTISMASH_ALTERNATIVE_BASE=$2; shift; shift ;;
        "") break;;
        *) display_help; exit 1;;
    esac
done

VERSION="antismash-2.1.0"

TMPDIR=/tmp

ARCH=$(uname -m)

BLAST_URL_ROOT="ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/"
BLAST_PACKAGE="ncbi-blast-2.2.29+-1.${ARCH}.rpm"

ANTISMASH_BASE_BITBUCKET="https://bitbucket.org/antismash/antismash2/downloads"
ANTISMASH_BASE=${ANTISMASH_ALTERNATIVE_BASE:-$ANTISMASH_BASE_BITBUCKET}
ANTISMASH_TARBALL="${VERSION}.${ARCH}.tar.bz2"

ANTISMASH_CFG="${HOME}/.antismash.cfg"

die() {
    echo $@
    exit 1
}

install_blast() {
    if [ ! $(which blastp) ]; then
        pushd $TMPDIR
        wget "${BLAST_URL_ROOT}${BLAST_PACKAGE}" || die "Failed to fetch blast package"
        sudo rpm -Uvh ${BLAST_PACKAGE}
        popd
    fi
}

install_hmmer3() {
    if [ ! $(which hmmscan) ]; then
        wget http://selab.janelia.org/software/hmmer3/3.0/hmmer-3.0.tar.gz
        tar xf hmmer-3.0.tar.gz
        pushd hmmer-3.0
        ./configure && make && make check && sudo make install
        popd
    fi
}

install_hmmer2() {
    if [ ! $(which hmmpfam2) ]; then
        wget http://selab.janelia.org/software/hmmer/2.3.2/hmmer-2.3.2.tar.gz
        tar xf hmmer-2.3.2.tar.gz
        pushd hmmer-2.3.2
        ./configure && make && make check
        sed -e "s#\(cp src/\$\$file \$(BINDIR)/\);#\1\$\${file}2;#" -i Makefile
        sudo make install
        popd
    fi
}

install_glimmer() {
    if [ ! $(which glimmer3) ]; then
        wget http://ccb.jhu.edu/software/glimmer/glimmer302.tar.gz
        tar xf glimmer302.tar.gz
        pushd glimmer3.02
        wget https://bitbucket.org/antismash/antismash2/downloads/Allow-glimmer-to-compile-on-g-4.4.3.patch
        patch -p1 < Allow-glimmer-to-compile-on-g-4.4.3.patch
        pushd src
        make
        popd
        for file in bin/*; do sudo cp $file /usr/local/bin; done
        popd
    fi
}

install_glimmerhmm() {
    if [ ! $(which glimmerhmm) ]; then
        wget ftp://ccb.jhu.edu/pub/software/glimmerhmm/GlimmerHMM-3.0.2.tar.gz
        tar xf GlimmerHMM-3.0.2.tar.gz
        pushd GlimmerHMM/sources
        make
        sudo cp glimmerhmm /usr/local/bin/
        popd
    fi
}

install_muscle() {
    if [ ! $(which muscle) ]; then
        wget http://www.drive5.com/muscle/downloads3.8.31/muscle3.8.31_src.tar.gz
        tar xf muscle3.8.31_src.tar.gz
        pushd muscle3.8.31/src
        make
        sudo cp muscle /usr/local/bin/
        popd
    fi
}

install_common() {
    sudo yum install -y perl-Archive-Tar python-pip python-virtualenv git \
                        java-1.7.0-openjdk python-devel libxslt-devel \
                        libxml2-devel gcc-c++ patch glibc-static cairo

    install_blast
    install_hmmer3
    install_hmmer2
    install_glimmer
    install_glimmerhmm
    install_muscle
}

install_6_4() {
    wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
    sudo rpm -Uvh epel-release-6-8.noarch.rpm
    install_common
}

get_release_number() {
    NUM=$(cat /etc/redhat-release | cut -d ' ' -f 3)
    echo ${NUM/\./_}
}

handle_install() {
    HELPER="install_$(get_release_number)"
    $HELPER || \
    die "Installer not set up for CentOS $(cat /etc/redhat-release | cut -d ' ' -f 3), please contact antismash@biotech.uni-tuebingen.de"
}

pip_or_die() {
    PACKAGE=$1
    pip install ${PACKAGE} || die "Failed to install ${PACKAGE}"
}

get_antismash() {
    pushd "${SOFTDIR}"
    virtualenv $VIRTENV_OPTIONS sandbox
    source sandbox/bin/activate
    pip install --upgrade pip
    pip_or_die argparse
    pip_or_die "straight.plugin==1.4.0-post-1"
    pip_or_die "cssselect==0.7.1"
    pip_or_die "lxml==3.2.3"
    pip_or_die "pyquery==1.2.4"
    pip_or_die numpy
    pip_or_die "biopython>=1.62"
    pip_or_die helperlibs
    pip_or_die pysvg
    pip_or_die pyExcelerator
    wget -N "${ANTISMASH_BASE}${ANTISMASH_TARBALL}"
    tar --strip-components=1 -xf ${ANTISMASH_TARBALL}
    python download_databases.py
    popd
}

######################
# Actually do the work
######################

handle_install

get_antismash

cat > ${ANTISMASH_CFG} <<EOF

EOF

cat > run_antismash <<EOF
#!/bin/bash
source $(pwd)/sandbox/bin/activate
$(pwd)/run_antismash.py \$*
EOF

chmod a+x run_antismash

echo "Done installing antiSMASH. To run, put the 'run_antismash' wrapper script into your path."
