#!/bin/bash

export PATH=${PATH}:/usr/local/bin

bash /vagrant/install_centos.sh -b http://dl.secondarymetabolites.org/
./run_antismash -d --smcogs --clusterblast --subclusterblast --full-hmmer /vagrant/balh.embl
pushd balh
iptables -F
python -m SimpleHTTPServer 8080
