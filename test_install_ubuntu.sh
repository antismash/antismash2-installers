#!/bin/bash

sudo apt-get update
sudo apt-get upgrade
bash /vagrant/install_ubuntu.sh -b http://dl.secondarymetabolites.org/
./run_antismash -d --smcogs --clusterblast --subclusterblast --full-hmmer /vagrant/balh.embl
pushd balh
python -m SimpleHTTPServer 8080
