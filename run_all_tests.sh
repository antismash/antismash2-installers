#!/bin/bash

AVAILABLE_VMS=$(grep config.vm.define Vagrantfile | cut -d"|" -f 2)
TESTDIR="webui"

for vm in $AVAILABLE_VMS; do
    echo "testing $vm"

    # remove stale test directory
    if [ -d "${TESTDIR}/current" ]; then
        rm -rf "${TESTDIR}/current"
    fi
    mkdir "${TESTDIR}/current"
    echo "running cool tests now"
    echo "{ 'name': '$vm'; 'result': 'success' }" > ${TESTDIR}/current/result.json

    # remove old results
    if [ -d "${TESTDIR}/${vm}" ]; then
        rm -rf "${TESTDIR}/${vm}"
    fi
    mv "${TESTDIR}/current" "${TESTDIR}/output_${vm}"
done
